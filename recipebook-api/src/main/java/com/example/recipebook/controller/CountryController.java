package com.example.recipebook.controller;

import java.util.List;
import java.util.Optional;

import com.example.recipebook.repository.CountryRepository;
import com.example.recipebook.service.CountryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.recipebook.model.Country;

@CrossOrigin("*")
@RestController
@RequestMapping("api/v1")
public class CountryController {
    // bringing in the country service
    @Autowired
    CountryService countryService;

    // creating a get method to GET the Countries from our database
    @GetMapping("/countries")
    public ResponseEntity<List<Country>> getAllCountries() {
        List<Country> countries = countryService.findAll();
        return new ResponseEntity<List<Country>>(countries, HttpStatus.OK);
    }

    // method to POST / create a country in the db
    @PostMapping("/countries")
    public ResponseEntity<Country> save(@RequestBody Country country) {
        Country newCountry = countryService.save(country);
        return new ResponseEntity<Country>(newCountry, HttpStatus.OK);
    } 

    // get method to GET the data of an individual country object
    // based off of the id's properties
    @GetMapping("/countries/{id}")
    public ResponseEntity<Country> getCountry(@PathVariable("id") Long id) {
        Optional<Country> countryData = countryService.findbyId(id);

		if (countryData.isPresent()) {
			return new ResponseEntity<>(countryData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
    }


    // delete method to delete the data of an individual country object
    // based off of the objects id
    @DeleteMapping("/countries/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        countryService.delete(id);
        return new ResponseEntity<String>("Country has been successfully deleted!", HttpStatus.OK);
    }

    @PutMapping("/countries/{id}")
	public ResponseEntity<Country> updateTutorial(@PathVariable("id") long id, @RequestBody Country country) {
		Optional<Country> countryData = countryService.findbyId(id);

		if (countryData.isPresent()) {
			Country _country = countryData.get();
			_country.setName(country.getName());
			return new ResponseEntity<>(countryService.save(_country), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}


}
