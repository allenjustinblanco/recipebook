package com.example.recipebook.repository;

import com.example.recipebook.model.Food;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FoodRepository extends JpaRepository<Food, Long> {
    
}
