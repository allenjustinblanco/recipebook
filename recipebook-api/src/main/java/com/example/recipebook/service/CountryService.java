package com.example.recipebook.service;

import java.util.List;
import java.util.Optional;

import com.example.recipebook.model.Country;

public interface CountryService {
    List<Country> findAll();

    // method to save
    Country save(Country country);

    // method to find an object by id
    Optional<Country> findbyId(Long id);

    // method to delete an object from the database
    void delete(long id);

}
