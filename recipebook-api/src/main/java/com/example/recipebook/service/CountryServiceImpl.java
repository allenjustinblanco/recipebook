package com.example.recipebook.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import com.example.recipebook.model.Country;
import com.example.recipebook.repository.CountryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountryServiceImpl implements CountryService, Serializable{
    
    @Autowired
    CountryRepository countryRepository;

    @Override
    public List<Country> findAll() {
        return countryRepository.findAll();
    }

    @Override
    public Country save(Country country) {
        countryRepository.save(country);
        return country;
    }

    @Override
    public Optional<Country> findbyId(Long id) {
        if(countryRepository.findById(id).isPresent()) {
            return countryRepository.findById(id);
        }

        return null;
    }

    @Override
    public void delete(long id) {
        countryRepository.deleteById(id);
    }


}
