import { Component, OnInit } from '@angular/core';
import { Country } from 'src/app/models/country.model';
import { CountryService } from 'src/app/services/country.service';

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.css']
})
export class AddCountryComponent implements OnInit {

  country: Country = {
    id: null,
    name: ''
  }

  submitted = false;

  constructor(private countryService: CountryService) { }

  ngOnInit(): void {
  }

  saveCountry(): void {
    const data = {
      name: this.country.name
    }

    this.countryService.create(data)
      .subscribe(
        res => {
          console.log(res);
          this.submitted = true;
        }, 
        error => {
          console.log(error)
      });
  }

  newCountry(): void {
    this.submitted = true;
    this.country = {
      id: null,
      name: ''
    }
  }

}
