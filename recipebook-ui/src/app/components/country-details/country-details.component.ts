import { Component, OnInit } from '@angular/core';
import { Country } from 'src/app/models/country.model';
import { CountryService } from 'src/app/services/country.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.css']
})
export class CountryDetailsComponent implements OnInit {

  currentCountry: Country = {
    id: undefined,
    name: '',
  };
  message = '';

  constructor(
    private CountryService: CountryService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.message = '';
    this.getCountry(this.route.snapshot.params.id);
  }

  getCountry(id: string): void {
    this.CountryService.get(id)
      .subscribe(
        data => {
          this.currentCountry = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  updatePublished(status: boolean): void {
    const data = {
      name: this.currentCountry.name,
    };

    this.message = '';

    this.CountryService.update(this.currentCountry.id, data)
      .subscribe(
        response => {
          console.log(response);
          this.message = response.message ? response.message : 'This Country was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  updateCountry(): void {
    this.message = '';

    this.CountryService.update(this.currentCountry.id, this.currentCountry)
      .subscribe(
        response => {
          console.log(response);
          this.message = response.message ? response.message : 'This Country was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  deleteCountry(): void {
    this.CountryService.delete(this.currentCountry.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/countries']);
        },
        error => {
          console.log(error);
        });
  }
}
