import { Component, OnInit } from '@angular/core';
import { CountryService } from '../../services/country.service';
import { Country } from 'src/app/models/country.model';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})
export class CountryListComponent implements OnInit {

  countries: Country[]; 
  currentCountry?: Country;
  currentIndex = -1;
  name = '';

  constructor(private countryService: CountryService) { }

  ngOnInit(): void {
    this.getCountries();
  }

  getCountries(): void {
    this.countryService.getAll()
      .subscribe(
        data => {
          this.countries = data;
        },
        error => {
          console.log(error);
        }
      )
  }

  refreshList(): void {
    this.getCountries();
    this.currentCountry = undefined;
    this.currentIndex = -1;
  }

  setActiveCountry(Country: Country, index: number): void {
    this.currentCountry = Country;
    this.currentIndex = index;
  }

  removeAllCountries(): void {
    this.countryService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.refreshList();
        },
        error => {
          console.log(error);
        });
  }

  searchName(): void {
    this.currentCountry = undefined;
    this.currentIndex = -1;

    this.countryService.findByName(this.name)
      .subscribe(
        data => {
          this.countries = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
